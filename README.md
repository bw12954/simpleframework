# Simple Framework
As the name suggests, this package is a simple framework that allows you to build a working web application framework easily.

## Usage
```
var framework = require("simpleframework");
framework.start();
```


## Setup
- `npm install` to install dependencies
- `gulp build` to prepare client side resources
- [Handlers](pages/README.md)
## Run
`node main.js`

